#include <iostream>
#include <cstdlib>
#include<cmath>

using namespace std;

void p1();
void p2();
void p3();

int main()
{
    int p = 0;
	while (p<1 || p>4){
        cout << "Men� de opciones, del 1-4" << endl;
		cout << "Problema 1"<<endl;
		cout << "Problema 2"<<endl;
		cout << "Problema 3"<<endl;
		cout << "Salir"<<endl;
        cout <<" "<<endl;
		cin >> p;
		}
    if(p == 1){
        p1();
    }
    else if(p == 2){
        p2();
    }
    else if(p == 3){
        p3();
    }
    else{
       return 0;
    }
    return 0;
    }

void p1()
{
    int cel = -1;
    int x = 0;
    int val = -1;
    int i = -1;
    int faren;
    while (cel <0){
        cout << "Valor en Celsius: " << endl;
        cin >> cel;
    }
    faren = (1.8 * cel) +32;
    while(val <0 || val >1000){
        cout << "N�mero de valores: "<<endl;
        cin >> val;
    }
    while(i<2 || i>10){
        cout << "N�mero de incremento: " <<endl;
        cin>> i;
    }
    cout << "Conversion de grados Celsius a Farenheit"<<endl;
    cout << "Farenheit \t Celsius"<<endl;
    cout << faren <<"\t\t "<< cel <<endl;

    for(x = 0; x <= val; x++)
    {
       cel += i;
       faren = 1.8 * cel +32;
       cout<< faren <<"\t\t "<< cel <<endl;
    }

}

void p2()
{
    cout << "Suma de una serie con incremento de 3" <<endl;
    int a = 1;
    int i = 3;
    int n = 25;
    int res = 0;
    int suma_final = 0;
    for(int i = 0; i < n; i++)
    {
        res = a + (i * i);
        suma_final = suma_final + res;
        cout << "Termino " << i+1 << ": " << res << endl;


    }
cout << "El resultado de la suma de la serie es de: " << suma_final << endl;
}

void p3()
{
    cout << "Numeros para conocer su media geometrica y armonica" << endl;
    cout << "Puede presionar 0 para terminar con el programa" <<endl;

    int v_nuev;
    int n = 0;
    int mult = 1;
    float cuenta = 0;
    while(true)
    {
        cin >> v_nuev;
        if(v_nuev != 0)
        {
            n++;
            mult *= v_nuev;
            cuenta += 1./v_nuev;
        }
        else
        {
             cout << "El resultado de la media geometrica es: " << pow(mult,1./n) << endl;
             cout << "El resultado de la media armoica es: " << n/(cuenta) << endl;
            exit(0);
        }

    }
}
